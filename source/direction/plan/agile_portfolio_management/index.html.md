---
layout: markdown_page
title: "Category Vision - Agile Portfolio Management"
---

- TOC
{:toc}

## Description
Large enterprises are continually working on increasingly more complex and larger
scope initiatives that cut across multiple teams and even departments, spanning months,
quarters, and even years. GitLab's vision is to allow organizing these initiatives 
into powerful **multi-level work breakdown** plans and enable **tracking the execution** 
of them over time, to be extremely simple and insightful. In addition, GitLab should 
highlight which **opportunities have higher ROI**, helping teams make strategic 
business planning decisions.

GitLab's **multi-level work breakdown** planning capabilities will include [multiple layers of epics](https://gitlab.com/groups/gitlab-org/-/epics/312) 
and [issues](https://gitlab.com/groups/gitlab-org/-/epics/316), allowing enterprises to capture:

- High-level strategic initiatives and OKRs (objectives and key results).
- Portfolios, programs, and projects.
- Product features and other improvements.
- [Work roll ups](https://gitlab.com/groups/gitlab-org/-/epics/312) and [dependency management](https://gitlab.com/groups/gitlab-org/-/epics/316).

GitLab will leverage [timeline-based roadmap visualizations](https://gitlab.com/gitlab-org/gitlab-ee/issues/7077)
to help enterprises plan from small time scales (e.g. 2 week sprints for development 
teams) to large time scales (e.g. annual strategic initiatives for entire departments).
Enhanced issues and [issue boards](https://gitlab.com/groups/gitlab-org/-/epics/293) 
[capabilities](https://gitlab.com/groups/gitlab-org/-/epics/383) will allow more 
versatile and powerful backlog grooming and cross-sprint planning functionality.

GitLab will enable teams to **track execution** of these plans over time. In addition
to roadmap views as well as existing time tracking and story point estimation features 
(called weights in GitLab), GitLab workflow management will also be improved with 
[group-level customized workflows integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/424),
as well as [burndown and burnup charts also integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/372).

GitLab will help teams **identify high ROI opportunities**, by organizing and surfacing
initiatives with relatively [large](https://gitlab.com/gitlab-org/gitlab-ee/issues/7718) 
[benefits](https://gitlab.com/gitlab-org/gitlab-ee/issues/8115) (as entered by team members) 
and relatively low costs (via rolled up effort estimates at the epic level).

GitLab supports [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/), 
including Scaled Agile Framework (SAFe), Scrum, and Kanban.

See how GitLab team-members themselves use GitLab Agile PPM to create GitLab, as
of GitLab 11.4: [https://www.youtube.com/watch?v=ME9VwseXMuo](https://www.youtube.com/watch?v=ME9VwseXMuo)

See [upcoming planned improvements for Agile portfolio and project management](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=EnterpriseAgile).

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorites for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

We now have the basic epic object working in GitLab. You can attach issues to epics and you can view epics in a roadmap visualization. The next step is to introduce more flexibility in epic relationship structures, to address more use cases. In particular, the next step is building out flexible work breakdown structures with epics of epics.

See https://gitlab.com/groups/gitlab-org/-/epics/312 and https://gitlab.com/groups/gitlab-org/-/epics/644.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Portfolio management is not a radically new industry. And there are established players such as Clarity, Planview, VersionOne, AgileCraft, and ServiceNow. Many of these older tools were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools typically have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. Our strategy is building _toward_ those enterprise use cases and abstractions, starting with the product-development baseline abstractions. And so we are developing multi-level child epics, and adding functionality there incrementally.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We are working with analysts to better understand the space of Agile Portfolio Management. For example, Gartner has an area called [Enteprise Agile Planning Tools](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendors), which this GitLab category is very much a part of. 

Analysts in this space are concerned with truly "enterprise" use cases. Agile methodologies have been around for a good number of years, but they have been focused on small teams, or maybe a small number of teams working closely in concert to execute and deliver features. The industry is now focused on how to take these processes that have been proven in small teams, and scale them to large enterprises with business initiatives that span potentially even multiple departments. What is crucial therefore, is how can organizations deliver business value very quickly, but still allow stakeholders (especially executive stakeholders) the visibility to track progress and be assured that the enterprise is working on the right initiatives in the first place. This has even led to the popular [SAFe (Scaled Agile Framework) methodology](https://www.scaledagileframework.com) as a way to help guide enterprise organizations in their Agile transformations.

At GitLab, we embrace this enterprise Agile transformation that companies are going through and want to lead the way by building the tools to do so. In particular, having a single application to view your entire work breakdown structure, from executive level, all the way down to issues and merge requests and even commits, means that our design truly allows for streamlining that visibility. We are focused on building this work breakdown structure out as the initial goal in Agile Portfolio Management.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The top feature in this area is indeed flexible work breakdown structures. We are working on that, along with tree-based and roadmap-based visualizations in https://gitlab.com/groups/gitlab-org/-/epics/312.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Many users are interested in harmonizing epics and issues together. The want "epics in projects" or "issues in groups". We are addressing that by solving each of those relevant use cases in turn. See https://gitlab.com/gitlab-org/gitlab-ce/issues/24536#note_125134359 further context. In particular, the issues are:

- Default project (issues) for group level free-flow discussion and collaboration: https://gitlab.com/gitlab-org/gitlab-ee/issues/8867
- Backlog management of epics: https://gitlab.com/gitlab-org/gitlab-ee/issues/8864
- Epic status or approval - From pitch to funded/sponsored to in execution https://gitlab.com/gitlab-org/gitlab-ee/issues/8865

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

GitLab team-members also want to have flexible work breakdown structures and have more power to manipulate issues and epics together, to meet various cross-functional needs. So https://gitlab.com/groups/gitlab-org/-/epics/312 continues to be the most important improvement here.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
- https://gitlab.com/groups/gitlab-org/-/epics/312 
- https://gitlab.com/groups/gitlab-org/-/epics/644
