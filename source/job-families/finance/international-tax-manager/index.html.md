---
layout: job_family_page
title: "International Tax Manager"
---

The International Tax Manager adds value to GitLab’s overall tax strategy including all components
of tax: meaning tax planning, tax compliance, tax assurance and accounting for income taxes. The succesful
candidate is a Jackie / Jack of All Trades. You report to the Director of Tax and will be part of an awesome
Finance Team in an all-remote, demanding high-growth environment.

## Responsibilities

* Play a significant role in establishing GitLab’s global tax strategy
* Tax Planning
  * Support the implementation of non-structural tax planning strategies
  * Participate in proposed Transactions to advise the Finance Team on Tax Consequences
  * Support USA and foreign tax integration of acquisitions, dispositions and restructurings into the corporate structure
* Transfer Pricing
  * Ensure adherance to the Transfer Pricing Concept
  * Maintain Transfer Pricing Documentation
* Tax Compliance
  * Ensure Global Corporate / State Income Tax Compliance
  * Closely work together with finance team members on VAT, Sales, Use Tax compliance
  * Ensure Global VAT compliance to electronic services
  * Take ownership of the control cycle to mitigate risks of Permanent Establishments
  * Take ownership of the cycle to mitigate employment tax risks
* Tax Audits
  * Evaluate requests and participate in developing strategies
  * Support the coordination and implementation of tax audit strategies
* Financial Reporting
  * Support preparation current / deferred income tax expense for financial statement purposes
  * Evaluate uncertain tax reporting positions and business combinations
  * Analyze the tax aspects of financial projections and forecasts
  * Lead technology and process improvements to ensure accurate, timely information is provided to the Finance team
  * Implementation and assurance of control cycle compliant with Sarbanes-Oxley
* Departmental liaison with IT staff on technical matters relating to tax applications

## Requirements

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* Relevant experience in a Big 4 environment and/or Business environment (preferrably both)
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* International tax experience required with preference for candidates in EMEA
* Ability to managing multiple projects at the same time

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates will then be invited to schedule an interview with our Director of Legal
* Next, candidates will be invited to schedule an interview with our External Tax Advisor
* Finally, candidates will have a 30min call with either our CEO or a GitLab Audit Committee Member

Additional details about our process, benefits and compensation can be found on our [hiring page](/handbook/hiring).
