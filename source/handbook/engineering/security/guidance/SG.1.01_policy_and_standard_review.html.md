---
layout: markdown_page
title: "SG.1.01 - Policy and Standard Review Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SG.1.01 - Policy and Standard Review

## Control Statement

GitLab's policies and standards are reviewed, updated if required, approved by management, and communicated to authorized personnel annually.

## Context

The purpose of this control is to ensure GitLab's policies and procedures are kept up-to-date and relevant, changes are appropriately reviewed and approved, and GitLab team members have a way to track those changes.

## Scope

Policies and Standards

## Ownership

Control Owner:

* Security Compliance

Process Owner:

* Security

## Guidance

Create process to have policies and standards reviewed and updated on a recurring, annual basis.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Policy and Standard Review control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/875).

## Framework Mapping

* ISO
  * A.5.1.1
  * A.5.1.2
  * A.12.1.1
  * A.12.5.1
  * A.12.6.2
* SOC2 CC
  * CCC1.4
  * CC2.1
  * CC2.3
  * CC3.1
  * CC3.2
  * CC5.1
  * CC5.2
  * CC5.3
* PCI
  * 1.5
  * 2.5
  * 3.5
  * 3.5.1
  * 3.5.2
  * 3.5.3
  * 3.5.4
  * 3.6
  * 3.6.1
  * 3.6.2
  * 3.6.3
  * 3.6.4
  * 3.6.5
  * 3.6.6
  * 3.6.7
  * 3.6.8
  * 4.3
  * 5.4
  * 6.7
  * 7.3
  * 8.1
  * 8.1.1
  * 8.1.2
  * 8.1.3
  * 8.1.4
  * 8.1.5
  * 8.1.6
  * 8.1.7
  * 8.1.8
  * 8.4
  * 8.8
  * 9.10
  * 9.10
  * 10.9
  * 11.6
  * 12.1.1
  * 12.4
