---
layout: markdown_page
title: "RuboCop"
---

## On this page
{:.no_toc}

- TOC
{:toc}

[RuboCop](http://www.rubocop.org/en/stable/) is used for running static code analysis on your [Ruby](https://www.ruby-lang.org/en/) code.

## "Magic" one-liners

### RuboCop-ing a set of files

You can combine the tips about listing files on the [Git page](/handbook/tools-and-tips/git/#listing-files) with RuboCop.

- To RuboCop the current commit - `git diff-tree --no-commit-id --name-only -r HEAD | xargs bundle exec rubocop`
- To RuboCop the working tree changes - `git diff --name-only | xargs bundle exec rubocop`
- To RuboCop all of the changes from the branch - `git diff --name-only master | xargs bundle exec`